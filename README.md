# Practico 7 - Transformación afín - Incrustando imágenes

[Transformacion_afin.py](https://gitlab.com/vision-por-computadora615916/practico-7-transformacion-afin-incrustando-imagenes/-/blob/main/Transformacion_afin.py?ref_type=heads) este script en Python utiliza OpenCV para permitir al usuario dibujar un rectángulo sobre una imagen, recortar la sección seleccionada y aplicar operaciones de traslación, rotación, escalado e incrustación de una segunda imagen mediante transformación afín.

## Funcionalidad

- Configuración Inicial: Importa las bibliotecas necesarias y define variables globales para el ángulo de rotación, desplazamiento y escala.

- Definición de Callback: Define la función draw_rectangule que maneja los eventos del ratón para iniciar, finalizar y dibujar el rectángulo, además de recortar la imagen seleccionada. También define la función draw_circle para seleccionar puntos.

- Funciones de Transformación: Define funciones para trasladar (translate), rotar (rotate) y realizar una transformación afín sobre la imagen.

- Carga de Imagen: Carga una imagen llamada `arco2.jpg` y otra llamada pelota.png para el procesamiento.

- Bucle Principal: En un bucle, muestra la imagen y escucha las teclas:

        q: Salir del programa.

        g: Guardar el recorte del rectángulo en `recorte.png.` 

        e: Aplicar traslación y rotación, y guardar el resultado en `traslado_rotacion.png`.

        s: Aplicar escalado y guardar el resultado en `escalado.png`.

        r: Reiniciar la imagen para empezar de nuevo.
        
        a: Seleccionar 3 puntos en la imagen para realizar una transformación afín e incrustar la segunda imagen.